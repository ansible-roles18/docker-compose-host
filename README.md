# docker-compose-host
Ansible role for deploying a docker host with compose installed

___
## Features
- [Adds the official Docker repo](https://docs.docker.com/engine/install/debian/#set-up-the-repository)
- [Installs Docker engine](https://docs.docker.com/engine/install/debian/#install-docker-engine) and docker-compose
- Creates /data directory for persistent container data
- Adds borgmatic config file that
  - stops Docker daemon
  - Backs up /data
  - Starts docker daemon
  - Sends email on failure
 

___
## Dependencies
- [debian-base](https://gitlab.com/ansible-roles18/debian-base)
- [borgmatic-client](https://gitlab.com/ansible-roles18/borgmatic-client)

___

## Example Playbook

This example includes variables from the other roles listed in [Dependencies](#dependencies)

    - hosts: all
      vars:
         borg_host: yourborghost.com
         borg_encryption_key: dontuseme
         admin_email: admin@mail.example
         gmail_account: outgoing_email@mail.example
         gmail_token: verysecuretokenindeed

      roles:
        - debian-base
        - borgmatic-client
        - docker-compose-host
        
